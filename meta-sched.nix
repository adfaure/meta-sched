{ stdenv, fetchFromGitHub, rustPlatform, makeWrapper, zeromq, pkgconfig}:

with rustPlatform;

buildRustPackage rec {
  name = "meta-sched-${version}";
  version = "1.1.0";

  propagatedBuildInputs = [
    pkgconfig
    zeromq
  ];

  src = ./.;
  depsSha256 = "1x52ca8n33mf4p3rlbv6hsc0z3c366smr7wash5iwbadzbmg6da4";
  cargoSha256 = "03vq1j1cha2y57k43b54rafadxkhhzs1vvycvrkmc09y5l9f0m2i";
}
