extern crate batsim;
extern crate env_logger;
extern crate interval_set;
extern crate uuid;
extern crate docopt;
#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate serde_json;
#[macro_use]
extern crate log;

use docopt::Docopt;

mod common;
mod meta_scheduler;
mod fcfs_scheduler;
mod sub_scheduler;


const USAGE: &'static str = "
Usage:
  meta-scheduler [--version]

Options:
  -h --help             Show this screen.
  --version             print version.
";

#[derive(Debug, Deserialize)]
struct Args {
    flag_version: bool,
}

static VERSION: &'static str = "1.2.1";

fn main() {
    env_logger::init().unwrap();
    let args: Args = Docopt::new(USAGE)
        .and_then(|d| d.deserialize())
        .unwrap_or_else(|e| e.exit());

    if args.flag_version {
        println!("{}", VERSION);
        return;
    }

    let mut scheduler = meta_scheduler::MetaScheduler::new();
    let mut batsim = batsim::Batsim::new(&mut scheduler);

    batsim.run_simulation().unwrap();
}
