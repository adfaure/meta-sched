use batsim::json_protocol::Job;
use interval_set::*;
use std::cell::Cell;
use std::cell::RefCell;
use std::rc::Rc;
use uuid::Uuid;
use std::hash::{Hash, Hasher};
use std::fmt;
use std::cmp;

pub trait SubScheduler {
    fn easy_back_filling(&mut self, current_time: f64) -> Vec<Rc<Allocation>>;
    fn get_uuid(&self) -> Uuid;
    fn schedule_jobs(
        &mut self,
        time: f64,
        is_rejection_possible: &Fn(Rc<Allocation>) -> bool,
    ) -> (Option<Vec<Rc<Allocation>>>, Option<String>);
    fn compute_pressure(&self, policy: Policy) -> f64;
    fn job_waiting(&mut self, time: f64, allocation: Rc<Allocation>);
    fn add_job(&mut self, allocation: Rc<Allocation>);
    fn job_finished(&mut self, finished_job: String);
    fn job_killed(&mut self, killed_job: String);
    fn job_killed_finished_in_time(&mut self, finished_job: String);
    fn job_launched(&mut self, time: f64, alloc: Rc<Allocation>) -> bool;
    fn job_backfilled(&mut self, time: f64, job: String);
    fn job_revert_allocation(&self, allocation: Rc<Allocation>);
    fn register_to_allocation(&self, allocation: Rc<Allocation>);
    fn end_schedule_phase(&mut self);
    fn allocate_job(&self, allocation: Rc<Allocation>) -> Rc<Allocation>;
}

#[derive(Clone)]
pub enum Policy {
    FCFS, // First Come First Serve
    SPF, // Shortest processing time first
}

#[derive(Clone)]
/// An allocation wrap a
/// job with data such as the resources allocated
/// and the time at which the jobs may starts.
/// `job`: The job is the batsim job
/// `nodes`: the current allocation.
/// `scheduled_launched_time`: the latest release date.
pub struct Allocation {
    pub job: Rc<Job>,
    pub nodes: RefCell<IntervalSet>,
    //TODO `scheduled_launched_time` Does not seems very English
    pub scheduled_launched_time: Cell<f64>,
    pub starting_time: Cell<f64>,
    pub running_groups: RefCell<Vec<Uuid>>,
}


/// Function to compare two allocation (into an Rc)
/// to order them as  spf (shortest processing time first).
pub fn spf_compare(a: &Rc<Allocation>, b: &Rc<Allocation>) -> cmp::Ordering {
    if a.job.walltime > b.job.walltime {
        return cmp::Ordering::Less;
    } else if a.job.walltime < b.job.walltime {
        return cmp::Ordering::Greater;
    } else {
        // I need a deterministic way to
        // order job that have the same walltime
        if a.job.id < b.job.id {
            return cmp::Ordering::Less;
        } else {
            return cmp::Ordering::Greater;
        }
    }
}

impl fmt::Debug for Allocation {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "[{}]walltime={}, interval: {} - missing: {} [starting time:{}]",
            self.job.id,
            self.job.walltime,
            self.nodes.borrow().clone(),
            self.nb_of_res_to_complete(),
            self.scheduled_launched_time.get()
        )
    }
}

impl Hash for Allocation {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.job.id.hash(state);
    }
}

impl PartialEq for Allocation {
    fn eq(&self, other: &Allocation) -> bool {
        self.job.id == other.job.id
    }
}

impl Eq for Allocation {}

impl Allocation {
    pub fn new(job: Rc<Job>) -> Allocation {
        Allocation {
            job: job.clone(),
            nodes: RefCell::new(IntervalSet::empty()),
            scheduled_launched_time: Cell::default(),
            starting_time: Cell::default(),
            running_groups: RefCell::new(vec![]),
        }
    }

    pub fn nb_of_res_to_complete(&self) -> u32 {
        self.job.res as u32 - self.nodes.borrow().size()
    }

    pub fn remove_resources(&self, interval: IntervalSet) {
        // We copy the current refcell interval
        // to be able to call `into_inner` and finally clone the interval
        let temp_interval: IntervalSet = self.nodes.clone().into_inner().clone();
        *self.nodes.borrow_mut() = temp_interval.difference(interval.clone());
    }

    /// This function is in fact mutable because we take advantage
    /// of the `RefCell` at this point.
    pub fn add_resources(&self, interval: IntervalSet) {
        // We copy the current refcell interval
        // to be able to call `into_inner` and finally clone the interval
        let temp_interval: IntervalSet = self.nodes.clone().into_inner().clone();
        *self.nodes.borrow_mut() = interval.union(temp_interval);
        if self.running_groups.borrow().len() > 1 {
            trace!(
                "Add resources: {:?} - Resources left {}",
                self,
                self.nb_of_res_to_complete()
            );
        }
    }

    pub fn add_group(&self, uuid: Uuid) {
        let mut hashset = self.running_groups.borrow_mut();
        hashset.push(uuid);
    }

    pub fn remove_group(&self, uuid: Uuid) {
        let mut hashset = self.running_groups.borrow_mut();
        hashset.retain(|x| x != &uuid);
    }

    pub fn update_scheduled_launched_time(&self, time: f64) {
        self.scheduled_launched_time.set(time);
    }

    // The objective of this function is to be able to
    // backfill and thus the function is designed to update
    // the estimated launching time (scheduled_launching_time in the code)
    // only if it is greater that the actual time. The current time will be the
    // max time we could wait.
    pub fn may_update_scheduled_launched_time(&self, time: f64) {
        if time > self.scheduled_launched_time.get() {
            self.scheduled_launched_time.set(time);
        }
    }

    pub fn get_nodes_clone(&self) -> IntervalSet {
        self.nodes.borrow().clone()
    }

    pub fn get_groups_clone(&self) -> Vec<Uuid> {
        self.running_groups
            .borrow()
            .iter()
            .map(|sched_id| sched_id.clone())
            .collect()
    }
}
