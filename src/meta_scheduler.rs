extern crate serde_json;

use batsim::*;
use interval_set::{Interval, ToIntervalSet};
use sub_scheduler::SubSchedulerRejection;
use fcfs_scheduler::SubSchedulerFcfs;
use common::*;
use std::str::FromStr;
use std::cell::Cell;
use std::rc::Rc;
use std::collections::{HashMap, HashSet};
use uuid::Uuid;

#[derive(Debug)]
pub struct RejectedJob {
    job_id: String,
    initial_job: Rc<Job>,
    resub_job: Rc<Job>,
    ack_received: Cell<bool>,
    finished: Cell<bool>,
}

pub struct MetaScheduler {
    nb_resources: u32, //Defines the total number of nodes available on the platform
    nb_group: i32, // The total number of group, this must be an input variable
    smallest_group_size: u32, //The size of the smallest group
    time: f64, // The current time of the simulation
    config: serde_json::Value,
    max_job_size: usize, //The max job size that the cluster can handle
    threshold: f64,
    greater_grp_size: usize, // The size of the bigger group
    schedulers: Vec<Uuid>, //
    rejection_scheduler: Uuid, // The UUid of the scheduler in charge of the rejected jobs
    schedulers_map: HashMap<Uuid, Box<SubScheduler>>,
    nb_dangling_resources: i32,
    profiles: HashMap<String, Profile>,
    jobs: HashMap<String, Rc<Allocation>>,
    // Store the rejected jobs in order to be resub.
    // The key is the id of the rejected job, not the original id.
    rejected_jobs: HashMap<String, Rc<RejectedJob>>,
}

impl MetaScheduler {
    pub fn new() -> MetaScheduler {
        MetaScheduler {
            nb_resources: 0,
            nb_group: 0,
            smallest_group_size: 0,
            time: 0f64,
            schedulers: vec![],
            rejection_scheduler: Uuid::new_v4(),
            schedulers_map: HashMap::new(),
            config: json!(null),
            max_job_size: 0,
            greater_grp_size: 0,
            nb_dangling_resources: 0,
            threshold: 0f64,
            profiles: HashMap::new(),
            jobs: HashMap::new(),
            rejected_jobs: HashMap::new(),
        }
    }

    fn construct_groups(&mut self, percentage_used_for_rej: f64, total_number_of_group: i32) {
        assert!(percentage_used_for_rej >= 0f64);
        assert!(percentage_used_for_rej <= 100f64);

        let rejection_size = (self.nb_resources as f64 * (percentage_used_for_rej / 100f64)) as u32;
        let resources = self.nb_resources - rejection_size;

        let rejection_first_node = self.nb_resources - rejection_size;
        info!(
            "rejection size is {}/{} there are {} resources.",
            rejection_size,
            self.nb_resources,
            resources
        );

        let mut rejection_set = if rejection_size == 0 {
            Interval::new(rejection_first_node - 1, self.nb_resources - 1).to_interval_set()
        } else {
            Interval::new(rejection_first_node, self.nb_resources - 1).to_interval_set()
        };


        info!("Number of group requested: {}", total_number_of_group);

        assert!(
            total_number_of_group as f64 <= (resources as f64).log(2f64).floor(),
            "This platform cannot handle {} groups. Max number of group is {}",
            total_number_of_group,
            (resources as f64).log(2f64).floor()
        );

        self.smallest_group_size =
            (resources as i32 / (2i32.pow(total_number_of_group as u32) - 1)) as u32;
        self.nb_group = total_number_of_group;

        info!("the smallest group size is {}", self.smallest_group_size);

        let mut nb_machines_used = 0;

        let policy = match self.config["rejection"]["primary_queue"].as_str().unwrap() {
            "SPF" => Policy::SPF,
            "FCFS" => Policy::FCFS,
            ref x => panic!("Unknow primary queue paramater: {}", x)
        };

        info!("Primary Queue mode: {}", &self.config["rejection"]["primary_queue"].to_string());


        for idx in 0..total_number_of_group as u32 {
            let (lower, upper) = (
                self.smallest_group_size * 2_u32.pow(idx) as u32 - 1,
                (self.smallest_group_size * 2_u32.pow(idx + 1) - 2) as u32,
            );

            let interval = Interval::new(
                lower - (self.smallest_group_size - 1),
                upper - (self.smallest_group_size - 1),
            );

            let scheduler: Box<SubScheduler> = Box::new(SubSchedulerRejection::new(
                interval.clone(),
                self.threshold,
                policy.clone(),
            ));

            nb_machines_used += interval.range_size();
            info!(
                "Group(n={}) uses {} nodes: {:?} id: {}",
                idx,
                interval.range_size(),
                interval,
                scheduler.get_uuid()
            );

            self.schedulers.insert(idx as usize, scheduler.get_uuid());
            self.schedulers_map.insert(scheduler.get_uuid(), scheduler);
        }

        //The size of the greatest group of resources.
        self.greater_grp_size =
            (self.smallest_group_size * (2u32).pow(total_number_of_group as u32 - 1)) as usize;
        info!(
            "The biggest group has {} resources ;)",
            self.greater_grp_size
        );

        self.nb_dangling_resources = (resources - nb_machines_used) as i32;
        info!(
            "We use only {} over {}: {}",
            nb_machines_used,
            resources,
            self.nb_dangling_resources
        );

        // If there is no nodes reserved for the rejection
        // we need to shift the indice of the last node of the cluster.
        rejection_set = if rejection_size == 0 {
            rejection_set.clone().union(
                Interval::new(nb_machines_used, self.nb_resources - 1)
                    .to_interval_set(),
            )
        } else {
            rejection_set.clone().union(
                Interval::new(nb_machines_used, self.nb_resources - rejection_size)
                    .to_interval_set(),
            )
        };

        if rejection_set.size() <= 0 {
            panic!("No resource available for rejection");
        }

        self.max_job_size = (self.nb_resources - rejection_set.size()) as usize;
        trace!(
            "The biggest job size that can be accepted is {}",
            self.max_job_size
        );

        let scheduler: Box<SubScheduler> =
            Box::new(SubSchedulerFcfs::new_with_interval_set(rejection_set));

        self.rejection_scheduler = scheduler.get_uuid();
        self.schedulers_map.insert(scheduler.get_uuid(), scheduler);
    }

    fn get_job_grp(&self, job: &Job) -> usize {
        let ratio: f64 = job.res as f64 / self.smallest_group_size as f64;
        if ratio < 1f64 {
            return 0;
        }
        let ret = (ratio).log(2f64).ceil() as usize;
        ret
    }
}

impl Scheduler for MetaScheduler {
    fn simulation_begins(
        &mut self,
        timestamp: &f64,
        nb_resources: i32,
        config: serde_json::Value,
    ) -> Option<Vec<BatsimEvent>> {
        info!("MainScheduler Initialized with {} resources", nb_resources);
        self.nb_resources = nb_resources as u32;
        // self.max_job_size = nb_resources as usize - 1;

        info!("The max job size accepted is {}", self.max_job_size);

        self.config = config;
        // We get the threshold from the configuration
        let threshold: i32 = i32::from_str(&self.config["rejection"]["threshold"].to_string())
            .unwrap();


        self.threshold = threshold as f64;
        info!("Threshold is set at: {}", threshold);

        let total_number_of_group =
            i32::from_str(&self.config["rejection"]["number_of_group"].to_string()).unwrap();

        let rejection_percentage = f64::from_str(&self.config["rejection"]["rejection_percentage"]
            .to_string()).unwrap();

        info!("Number of groups: {}", total_number_of_group);
        info!("Percentage used for rejection: {}", rejection_percentage);
        self.construct_groups(rejection_percentage, total_number_of_group);

        // We tell batsim that it does not need to wait for us
        Some(vec![
            notify_event(
                *timestamp,
                String::from("submission_finished")
            ),
        ])
    }

    fn on_job_submission(
        &mut self,
        timestamp: &f64,
        job: Job,
        profile: Option<Profile>,
    ) -> Option<Vec<BatsimEvent>> {
        if job.res > self.max_job_size as i32 {
            trace!("Job too big: {:?}", job);
            return Some(vec![reject_job_event(*timestamp, &job)]);
        }

        // We save the profile of the new job
        match profile {
            Some(p) => {
                self.profiles.insert(job.profile.clone(), p);
            }
            None => panic!("Did you forget to activate the profile forwarding ?"),
        }

        trace!("Get a new job(={:?}) with size {}", job, job.res);
        let job_rc = Rc::new(job);
        let shared_allocation = Rc::new(Allocation::new(job_rc.clone()));

        self.jobs.insert(
            shared_allocation.job.id.clone(),
            shared_allocation.clone(),
        );

        self.register_schedulers(shared_allocation.clone());
        self.schedulers_for(shared_allocation.clone(), &mut |scheduler| {
            scheduler.add_job(shared_allocation.clone())
        });
        None
    }

    fn on_job_completed(&mut self, _: &f64, job_id: String, _: String) -> Option<Vec<BatsimEvent>> {
        trace!("Job completed: {}", job_id);
        // Finished job is an `Allocation`
        let finished_job = self.jobs
            .get(&job_id)
            .ok_or("No job registered with this id")
            .unwrap()
            .clone();
        {
            // Check if the job was previously rejected.
            // if it was the case, we set the variable to
            // note the information
            match self.rejected_jobs.get(
                &MetaScheduler::job_id_to_rej_id(&job_id),
            ) {
                Some(rejected_job) => rejected_job.finished.set(true),
                None => {}
            }
        }

        self.schedulers_for(finished_job.clone(), &mut |scheduler| {
            scheduler.job_finished(finished_job.job.id.clone());
        });

        None
    }

    fn on_simulation_ends(&mut self, timestamp: &f64) {
        info!("Simulation ends: {}", timestamp);
    }

    fn on_job_killed(&mut self, timestamp: &f64, jobs: Vec<String>) -> Option<Vec<BatsimEvent>> {
        let mut events: Vec<BatsimEvent> = vec![];
        'job: for job_id in jobs {
            trace!("Job {} has been successfully killed", &job_id);
            let killed_job: Rc<Allocation> = self.jobs
                .get(&job_id)
                .ok_or("No job registered with this id")
                .unwrap()
                .clone();

            {
                // In some case a job can be rejected, but finishes before
                // the kill occurs. In this case, we do not need to re sub the Job.
                let resub = self.rejected_jobs
                    .get(&MetaScheduler::job_id_to_rej_id(&job_id))
                    .unwrap()
                    .clone();

                if resub.finished.get() {
                    self.rejected_jobs.remove(
                        &MetaScheduler::job_id_to_rej_id(&job_id),
                    );
                    continue 'job;
                }
            }

            self.schedulers_for(killed_job.clone(), &mut |scheduler| {
                scheduler.job_killed(job_id.clone())
            });

            let resub = self.rejected_jobs
                .get(&MetaScheduler::job_id_to_rej_id(&job_id))
                .unwrap();

            let profile: Option<&Profile> = self.profiles.get(&killed_job.job.profile);

            trace!("Submit job {:?}", resub.resub_job);
            events.push(submit_job_event(*timestamp, &resub.resub_job, profile));
        }
        Some(events)
    }

    fn on_message_received_end(&mut self, timestamp: &mut f64) -> Option<Vec<BatsimEvent>> {
        // All events that will be send to batsim (all allocation that are ready)
        let mut events: Vec<BatsimEvent> = vec![];
        let mut all_allocations: Vec<Rc<Allocation>> = vec![];

        //We call a standard scheduler on each sub groups
        let (jobs, rejected) = self.schedule_jobs();
        let bf = self.easy_backfilling();

        // In the first place we call for a normal schedule on each scheduler.
        {
            for scheduler_id in &self.schedulers {
                let scheduler = self.schedulers_map.get_mut(scheduler_id).expect(
                    "No scheduler for the given uuid",
                );
                scheduler.end_schedule_phase();
            }
        }

        // We add all events to the allocation array
        all_allocations.extend(jobs);
        all_allocations.extend(bf);

        events.extend(MetaScheduler::allocations_to_batsim_events(
            self.time,
            all_allocations,
        ));

        if !rejected.is_empty() {
            events.push(batsim::kill_jobs_event(
                self.time,
                rejected.iter().map(|alloc| &*alloc.job).collect(),
            ));
        }

        trace!(
            "Respond to batsim at: {} with {} events",
            timestamp,
            events.len()
        );

        Some(events)
    }

    fn on_message_received_begin(&mut self, timestamp: &f64) -> Option<Vec<BatsimEvent>> {
        trace!("Received new batsim message at {}", timestamp);
        None
    }
}

impl MetaScheduler {
    fn easy_backfilling(&mut self) -> Vec<Rc<Allocation>> {
        let mut all_allocations: HashSet<Rc<Allocation>> = HashSet::new();

        all_allocations.extend(self.backfill_rejection_scheduler());
        for scheduler_id in &self.schedulers {
            let scheduler = self.schedulers_map.get_mut(scheduler_id).expect(
                "No scheduler for the given uuid",
            );

            let mut allocations = scheduler.easy_back_filling(self.time);
            // We arbitrary get one job (if there is one)
            match allocations.pop() {
                Some(alloc) => {
                    all_allocations.insert(alloc);
                }
                _ => {}
            }
        }

        let time: f64 = self.time;
        for alloc in &all_allocations {
            self.schedulers_for(alloc.clone(), &mut |scheduler| {
                scheduler.job_backfilled(time, alloc.job.id.clone());
            });
        }

        all_allocations.into_iter().collect()
    }

    fn backfill_rejection_scheduler(&mut self) -> Vec<Rc<Allocation>> {
        let scheduler = self.schedulers_map
            .get_mut(&self.rejection_scheduler)
            .expect("No scheduler for the given uuid");

        scheduler.easy_back_filling(self.time)
    }

    // Special function for the rejection scheduler that is
    // not allowed to reject job.
    fn schedule_rejection_scheduler(&mut self) -> Vec<Rc<Allocation>> {
        let mut all_allocations: Vec<Rc<Allocation>> = vec![];

        {
            let scheduler = self.schedulers_map
                .get_mut(&self.rejection_scheduler)
                .expect("No scheduler for the given uuid");

            let cb = |_: Rc<Allocation>| false;
            let (allocations, rejected) = scheduler.schedule_jobs(self.time, &cb);
            match allocations {
                Some(allocs) => {
                    all_allocations.extend(allocs);
                }
                _ => {}
            }

            match rejected {
                Some(_) => panic!("This scheduler is not allowed to reject jobs"),
                None => {}
            }
        }

        all_allocations
    }

    /// Function responsible for the scheduling of all groups defined from.
    fn schedule_jobs(&mut self) -> (Vec<Rc<Allocation>>, Vec<Rc<Allocation>>) {
        // Array that will holds every job that sub-scheduler are ready to run.
        let mut all_allocations: HashSet<Rc<Allocation>> = HashSet::new();
        let mut rejected_jobs: Vec<Rc<Allocation>> = vec![];

        let dang = self.nb_dangling_resources;
        let cb = |j: Rc<Allocation>| j.job.res <= dang;

        // In the first place we call for a normal schedule on each scheduler.
        {
            for scheduler_id in &self.schedulers {
                let scheduler = self.schedulers_map.get_mut(scheduler_id).expect(
                    "No scheduler for the given uuid",
                );

                // Call to the current subgroup schedule.
                // The scheduler may also reject a job.
                let (allocations, rejected) = scheduler.schedule_jobs(self.time, &cb);

                // Add new allocation to the array
                match allocations {
                    Some(allocs) => {
                        all_allocations.extend(allocs);
                    }
                    _ => {}
                }

                match rejected {
                    Some(job_id) => {
                        rejected_jobs.push(self.jobs.get(&job_id).unwrap().clone());
                    }
                    None => {}
                }
            }
        }

        // Schedule phase for the rejection scheduler.
        all_allocations.extend(self.schedule_rejection_scheduler());

        // For each rejected job we duplicate the job to
        // create a special job that will be identical to
        // to initial job. This code is very specific to
        // the usage of batsim that does not allow to "re-launch"
        // a terminated/killed job. So we create a job that have the
        // same characteristics.
        for reject in &rejected_jobs {
            trace!("Job {:?} has been rejected", reject);
            let rej_job = MetaScheduler::construct_rejected_job(reject.clone());
            self.rejected_jobs.insert(
                rej_job.job_id.clone(),
                Rc::new(rej_job),
            );
        }

        // We divide jobs into to subsets:
        // The jobs ready to be launched and
        // the jobs that still requires nodes.
        let (ready_allocations, delayed_allocations): (Vec<Rc<Allocation>>,
                                                       Vec<Rc<Allocation>>) =
            all_allocations.into_iter().partition(|alloc| {
                alloc.nb_of_res_to_complete() == 0
            });


        // For each delayed jobs we notify the
        // sub-schedulers that the jobs are delayed.
        // The sub-schedulers need to fix a release date
        // for the waiting jobs so we can backfill.
        let time: f64 = self.time;

        for delayed_allocation in &delayed_allocations {
            delayed_allocation.update_scheduled_launched_time(time);
            trace!(
                "missing {} for job - {} ",
                delayed_allocation.nb_of_res_to_complete(),
                delayed_allocation.job.id
            );
            self.schedulers_for(delayed_allocation.clone(), &mut |scheduler| {
                scheduler.job_waiting(time, delayed_allocation.clone())
            });
        }

        // We notify every scheduler whereas we launch one
        // of the jobs.
        for ready_allocation in &ready_allocations {
            trace!("Jobs has been launched: {:?}", ready_allocation);
            ready_allocation.starting_time.set(time);
            self.schedulers_for(ready_allocation.clone(), &mut |scheduler| {
                if !scheduler.job_launched(time, ready_allocation.clone()) {
                    ready_allocation.remove_group(scheduler.get_uuid());
                    trace!("Job {} removed from group", ready_allocation.job.id);
                }
            });
        }
        (ready_allocations.into_iter().collect(), rejected_jobs)
    }

    fn schedulers_for(
        &mut self,
        allocation: Rc<Allocation>,
        func: &mut FnMut(&mut Box<SubScheduler>),
    ) {
        for scheduler_id in &mut allocation.get_groups_clone().iter() {
            let scheduler = self.schedulers_map.get_mut(&scheduler_id).unwrap();
            func(scheduler);
        }
    }

    fn register_schedulers(&mut self, allocation: Rc<Allocation>) {
        let job = allocation.job.clone();
        let (w_id, _) = Job::split_id(&job.id);

        match w_id.as_ref() {
            "rej!" => {
                trace!(
                    "Rejected job, register rej sched {}",
                    self.rejection_scheduler
                );
                allocation.add_group(self.rejection_scheduler.clone());
            }
            _ => {
                if job.res <= self.greater_grp_size as i32 {
                    let grp_idx = self.get_job_grp(&job);
                    let uuid = self.schedulers.get(grp_idx).unwrap();
                    let scheduler = self.schedulers_map.get_mut(&uuid).expect(
                        "This is a bug :)",
                    );
                    scheduler.register_to_allocation(allocation.clone());
                } else {
                    // If the jobs do not fit into any of the sub schedulers
                    // we send it to schedulers till we have enough cores.
                    let mut iter_sched = self.schedulers.iter_mut().enumerate().rev();
                    let mut sched = iter_sched.next();
                    let mut cores_remaining = job.res;

                    trace!("job res: {} - {}", allocation.job.id, allocation.job.res);
                    while cores_remaining > 0 {
                        match sched {
                            Some(idx_scheduler) => {
                                let scheduler = self.schedulers_map
                                    .get_mut(idx_scheduler.1)
                                    .expect("This is a bug :0");

                                allocation.add_group(scheduler.get_uuid());
                                trace!(
                                    "Get jobs {} gets {} from group ",
                                    allocation.job.id,
                                    (self.smallest_group_size as i32 *
                                        2i32.pow(idx_scheduler.0 as u32))
                                );

                                cores_remaining -= self.smallest_group_size as i32 *
                                    2i32.pow(idx_scheduler.0 as u32);
                            }
                            None => {
                                if cores_remaining > 0 {
                                    trace!(
                                        "Job also needs resources from rej: {:?} - {}",
                                        allocation,
                                        self.rejection_scheduler.clone()
                                    );
                                    allocation.add_group(self.rejection_scheduler.clone());
                                }
                                break;
                            }
                        }
                        sched = iter_sched.next();
                    }
                }
            }
        }
    }

    fn job_id_to_rej_id(id: &String) -> String {
        let (_, job_id) = Job::split_id(&id);
        format!("rej!{}", job_id)
    }

    fn construct_rejected_job(allocation: Rc<Allocation>) -> RejectedJob {
        let (_, job_id) = Job::split_id(&allocation.job.id);

        let mut resub_job: Job = (*allocation.job).clone();
        resub_job.id = format!("rej!{}", job_id);

        trace!("{:?}", resub_job);
        RejectedJob {
            job_id: resub_job.id.clone(),
            initial_job: Rc::new((*allocation.job).clone()),
            resub_job: Rc::new(resub_job.clone()),
            ack_received: Cell::new(false),
            finished: Cell::new(false),
        }
    }

    fn allocations_to_batsim_events(now: f64, allocation: Vec<Rc<Allocation>>) -> Vec<BatsimEvent> {
        allocation
            .into_iter()
            .map(|alloc| {
                return allocate_job_event(now, &*alloc.job, format!("{}", *alloc.nodes.borrow()));
            })
            .collect()
    }
}
