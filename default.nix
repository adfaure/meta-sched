{
  pkgs ? import <nixpkgs> {},
}:
let
  callPackage = pkgs.lib.callPackageWith (pkgs // pkgs.xlibs);
in rec {
  meta-sched = callPackage ./meta-sched.nix { };
}
